# TC Voting App (ARA) TABANG GAMAY :D

A Voting App for TC ARA Elections this 2020. This is also to practice everyone in utilizing the best practices of using Git/Gitlab as a Version Control.

Create a Gitlab account. Gitlab is a ripoff of Github only better. :D

This has 2 parts

- [x] Election for Officers
- [ ] Election for DRs (District Representatives)

## Calling for the assistance of all powerful Frontend Developers!!!
If you have observations/improvements in the **USER INTERFACE** that you want to apply. Kindly do so and submit a PR.
Any help will be much appreciated.

>fork this Gitlab repository, then push your changes. Kindly see the **Issues Tab** for perceived improvements. You are welcome to add your own.
>Again, ANY PR (Pull Request) for UI enhancement is greatly appreciated. Love lots. :*


## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

