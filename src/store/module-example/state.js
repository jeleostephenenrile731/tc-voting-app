export default function () {
  return {
    currentUser: {},
    bodCandidatesList: [
      {
        id: 1,
        displayName: 'Mia Khalifa',
        imgUrl: 'https://live.staticflickr.com/65535/50798370727_caf72598cf_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/gwIcgyqioPo?rel=0',
        birthday: '1/1/1992',
        gender: 'Female',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },

      {
        id: 2,
        displayName: 'Imbah Jezzy',
        imgUrl: 'https://live.staticflickr.com/65535/50798252776_d9429f3c3b_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/KQdp0FlU5VE?rel=0',
        birthday: '12/10/1991',
        gender: 'Male',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },

      {
        id: 3,
        displayName: 'Sasha Gray',
        imgUrl: 'https://live.staticflickr.com/65535/50798370642_35b2a5d8d2_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/uP7MBS4MlVI?rel=0',
        birthday: '2/18/1992',
        gender: 'Female',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },

      {
        id: 4,
        displayName: 'Lapu Lapu',
        imgUrl: 'https://live.staticflickr.com/65535/50798370642_35b2a5d8d2_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/uP7MBS4MlVI?rel=0',
        birthday: '2/18/1992',
        gender: 'Female',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },
    ],
    elecomCandidatesList: [
      {
        id: 1,
        displayName: 'Anya Taylor-Joy',
        imgUrl: 'https://live.staticflickr.com/65535/50798840896_f96e827f0f_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/gwIcgyqioPo?rel=0',
        birthday: '1/1/1992',
        gender: 'Female',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },

      {
        id: 2,
        displayName: 'Azalea Samantha Diaz',
        imgUrl: 'https://live.staticflickr.com/65535/50799446806_66bb13fe72_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/KQdp0FlU5VE?rel=0',
        birthday: '12/10/1991',
        gender: 'Female',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },

      {
        id: 3,
        displayName: 'Mcbell Cadao',
        imgUrl: 'https://live.staticflickr.com/65535/50798957862_57c74ffa3d_c.jpg',
        videoUrl: 'https://www.youtube.com/embed/uP7MBS4MlVI?rel=0',
        birthday: '2/18/1992',
        gender: 'Male',
        isVoted: false,
        caption: 'Hi Ako si Mia Khalifa your girl next door. Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.Hi Ako si Mia Khalifa your girl next door.',
      },

    ],
    treasuryCandidatesList: [],
    castedVotes: {
      bodVotes: [],
      elecomVotes: [],
      treasuryVotes: []
    },

    //settings
    maxBodVotes: 3,
    maxTreasuryVotes: 2,
    maxElecomVotes: 2,

    defaultUsers: [
      {
        vrn: '000123',
        pin: '123123',
        name: 'HAROLD CALIO'
      },

      {
        vrn: '000124',
        pin: '123123',
        name: 'MCBELL CADAO'
      },

      {
        vrn: '000125',
        pin: '123123',
        name: 'CARMELITO BATINO'
      },
    ],
    //
  }
}
