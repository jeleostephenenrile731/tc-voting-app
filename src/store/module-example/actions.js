export function someAction (/* context */) {
}


export function loginCurrentUser({commit, state}, value) {
    commit('loginCurrentUser', value);
}


export function toggleVoteValue({commit, state}, value) {
    commit('toggleVoteValue', value);
}